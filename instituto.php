<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
}); 
use clases\Persona;
use clases\Profesor;
use clases\Estudiante;
use clases\Materias;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
       
        $objeto= new Persona([
            'Nombre'=>'Mi primer alumno',
            'Edad'=>'25',
        ]);
         echo 'objeto persona: '; var_dump($objeto);
        
        $estudiante= new Estudiante([
           'Nombre'=> 'alumno2',
           'Edad'=>25,
           'Notas'=>[4],
           'Carrera'=>'Matematicas',
        ]);
        
         echo 'objeto estudiante: ';var_dump($estudiante);
        
        
        $profe= new Profesor([
            'Nombre'=>'Profe1',
            'Edad'=>47,
            'Materia'=>'Inglés',
            'Sueldo'=>[1=>1700,2=>1700,5=>2000],
        ]);
        
        echo 'objeto profe: ';var_dump($profe);
        
        ?>
    </body>
</html>
