<?php

namespace clases;
 
class Persona {
    private $nombre;
    private $edad;
    
    function getNombre() {
        return $this->nombre;
    }

    function getEdad() {
        return $this->edad;
    }

    function setNombre(string $nombre): void {
        $this->nombre = $nombre;
    }

    function setEdad( int $edad): void {
        $this->edad = $edad;
    }

        
    function __construct(array $argumentos) {
        
        /* opcion 1
        foreach ($argumentos as $indice=>$valor){
            call_use_func([$this,'set'.$indice], $valor);
        }
         fin de opcion 1 */
        
        $this->setEdad($argumentos['Edad']);
        $this->setNombre($argumentos['Nombre']);
        
    }

    public function estudios() {
        return "mis estudios son...";
        
    }
    
}
