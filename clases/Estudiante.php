<?php
namespace clases;


class Estudiante extends Persona {
    public $carrera;
    private $notas;
    
    function __toString() {
        return 'Soy un estudiante ';
    }

    function getCarrera() {
        return $this->carrera;
    }

    
    function getNotas() {
        
        return join(',',$this->notas);
    }
    
    
    function setCarrera(string $carrera){
        $this->carrera = $carrera;
    }

        
    function setNotas($notas){
        
        $this->notas= $notas;    
        
        
    }
    
/*    public function __construct($nombre,$edad, array $notas=[], string $carrera='') {
        $this->setCarrera($carrera);
        $this->setNotas($notas);
        parent::__construct($nombre, $edad);
        
    } */
    
    public function __construct(array $argumentos=[]) {
        $opcionales=[
            'Nombre'=>'',
            'Edad'=>0,
            'Notas'=>[],
            'Carrera'=>'',
        ];
        
        /* lógica de negocio */
        $lleno=array_merge($opcionales, $argumentos);
        
        
        $this->setCarrera($lleno['Carrera']);
        $this->setNotas($lleno['Notas']);
        
        parent::__construct([
            'Edad'=>$lleno['Edad'],
            'Nombre'=>$lleno['Nombre']
            ]);
        
        
        
    }
    


}
